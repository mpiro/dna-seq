#!/bin/sh
#$ -S /bin/sh
#$ -cwd
#$ -l h_vmem=5G,time=8::

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi


echo "============== 05-1 picard fixmate ================"
echo " "
date

echo "Fixing the mate pairs of realigned reads"


mkdir -p ${tmp_folder}_fixmate 

java -Xmx${heap}m -Djava.io.tmpdir\=${tmp_folder}_fixmate \
  -jar ${picard}/FixMateInformation.jar \
  INPUT\=$PWDS/${subjectID}.srt.bam \
  OUTPUT\=$PWDS/${subjectID}.fxmt.bam \
  SO\=coordinate \
  CREATE_INDEX\=true  \
  VALIDATION_STRINGENCY\=SILENT


${bamtools} stats \
   -insert \
   -in $PWDS/${subjectID}.fxmt.bam \
   > $PWDS/${subjectID}.fxmt.stats


rm -rf ${tmp_folder}_fixmate

echo "Fixing mate pairs completed!"
date




