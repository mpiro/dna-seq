export subjectID=$1
export working_dir_path="$2"
export fastq_path1="$3"

export fastq_for_gz="${subjectID}_1.fastq.gz"
export fastq_rev_gz="${subjectID}_2.fastq.gz"
export lanes="1"
export run_script="/md2/01-script/illuminizer_v8_bsi"

export PWDS="$working_dir_path/analysis"
log="$working_dir_path/run.log"
log2="$working_dir_path/run.steps"

. ${run_script}/global_config.sh


 for (( i=1;i<=${lanes};i++ ))
 do
 
 . ${run_script}/01-bwa_mapping_$i.sh  \
   2>&1| tee -a $log
 echo "01-bwa_mapping_$i.sh is done - starting 02-sam_to_bam_$i.sh" >> $log2 

 . ${run_script}/02-sam_to_bam_$i.sh   \
   2>&1| tee -a $log
 echo "02-sam_to_bam_$i.sh is done - starting 03-samtools_index_$i.sh" >> $log2 

 . ${run_script}/03-samtools_index_$i.sh   \
   2>&1| tee -a $log
 echo "03-samtools_index_$i.sh is done - starting 04-1-samtools_merge.sh" >> $log2 
 
 done
 

 
 . ${run_script}/04-1-samtools_merge.sh   \
   2>&1| tee -a $log
echo "04-1-samtools_merge.sh is done - starting 04-2-samtools_index.sh" >> $log2

 . ${run_script}/04-2-samtools_index.sh   \
   2>&1| tee -a $log
echo "04-2-samtools_index.sh is done - starting 05-1-picard_fixmate.sh" >> $log2

 . ${run_script}/05-1-picard_fixmate.sh   \
   2>&1| tee -a $log
echo "05-1-picard_fixmate.sh is done - starting 05-2-bamtools_filter.sh" >> $log2

 . ${run_script}/05-2-bamtools_filter.sh   \
   2>&1| tee -a $log
echo "05-2-bamtools_filter.sh is done - starting 05-3-picard_remove_duplicates.sh" >> $log2

 . ${run_script}/05-3-picard_remove_duplicates.sh   \
   2>&1| tee -a $log
echo "05-3-picard_remove_duplicates.sh is done - starting 05-4-samtools_max_mapping_qual.sh" >> $log2

 . ${run_script}/05-4-samtools_max_mapping_qual.sh   \
   2>&1| tee -a $log
echo "05-4-samtools_max_mapping_qual.sh is done - starting 06-1-picard_fixmate.sh" >> $log2

 . ${run_script}/06-1-picard_fixmate.sh   \
   2>&1| tee -a $log
echo "06-1-picard_fixmate.sh is done - starting 06-2-bamtools_stats.sh" >> $log2

 . ${run_script}/06-2-bamtools_stats.sh   \
   2>&1| tee -a $log
echo "06-2-bamtools_stats.sh is done - starting 06-3-picard_validate.sh" >> $log2

 . ${run_script}/06-3-picard_validate.sh   \
   2>&1| tee -a $log
echo "06-3-picard_validate.sh is done - starting 11-1-gatk_creating_intervals.sh" >> $log2

 . ${run_script}/11-1-gatk_creating_intervals.sh   \
   2>&1| tee -a $log
echo "11-1-gatk_creating_intervals.sh is done - starting 11-2-gatk_realign.sh" >> $log2

 . ${run_script}/11-2-gatk_realign.sh   \
   2>&1| tee -a $log
echo "11-2-gatk_realign.sh is done - starting 11-3-picard_fixmate.sh" >> $log2

 . ${run_script}/11-3-picard_fixmate.sh   \
   2>&1| tee -a $log
echo "11-3-picard_fixmate.sh is done - starting 11-4-picard_sort.sh" >> $log2

 . ${run_script}/11-4-picard_sort.sh   \
   2>&1| tee -a $log
echo "11-4-picard_sort.sh is done - starting 12-1-gatk_analyze_covar.sh" >> $log2

 . ${run_script}/12-1-gatk_analyze_covar.sh   \
   2>&1| tee -a $log
echo "12-1-gatk_analyze_covar.sh is done - starting 12-2-gatk_recalibrate.sh" >> $log2

 . ${run_script}/12-2-gatk_recalibrate.sh   \
   2>&1| tee -a $log
echo "12-2-gatk_recalibrate.sh is done - starting 12-3-gatk_recalculate_analyze_covar.sh" >> $log2

 . ${run_script}/12-3-gatk_recalculate_analyze_covar.sh   \
   2>&1| tee -a $log
echo "12-3-gatk_recalculate_analyze_covar.sh is done - starting 12-4-gatk_dept_of_coverage.sh" >> $log2

 . ${run_script}/12-4-gatk_dept_of_coverage.sh   \
   2>&1| tee -a $log
echo "12-4-gatk_dept_of_coverage.sh is done - starting 12-5-picard_hsmetrics " >> $log2

 . ${run_script}/12-5-picard_hsmetrics.sh   \
   2>&1| tee -a $log
echo "12-5-picard_hsmetrics.sh is done - starting cleaning " >> $log2

 . ${run_script}/14-5-cleanup.sh   \
   2>&1| tee -a $log
echo "14-5-cleanup.sh is done - starting 15-1-variant-calling-gatk.sh " >> $log2

 . ${run_script}/15-1-variant-calling-gatk.sh $working_dir_path  $subjectID \
   2>&1| tee -a $log
echo "15-1-variant-calling-gatk.sh is done - pipeline completed" >> $log2


