#!/bin/sh
#$ -S /bin/sh
#$ -cwd
#$ -l h_vmem=5G,time=8::

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi

echo "============== 12-4 GATK Dept of Coverage  ================"
echo " "


 date
 mkdir -p ${tmp_folder}_dept 
 
    java -Xmx${heap}m -Djava.io.tmpdir\=${tmp_folder}_dept \
     -jar $gatk \
     -T DepthOfCoverage \
     -L $ExonFile \
     -l INFO \
     -R $REF \
     -I $PWDS/${subjectID}.realigned.recal.bam \
     -o $PWDS/${subjectID}.coverage.dept  \
      -L $ExonFile


  rm -rf ${tmp_folder}_dept
  mkdir -p $PWDS/coverage
  mv $PWDS/${subjectID}.coverage.dept* $PWDS/coverage/

echo "depth of coverage completed!"
date







