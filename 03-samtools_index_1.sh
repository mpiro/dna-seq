#!/bin/bash
#$ -cwd

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi

echo "============== 03- samtools indexing ================"
echo " "

# indexing bam
${samtools} index ${PWDS}/${subjectID}.$lane_num1.aligned.srt.bam
 
