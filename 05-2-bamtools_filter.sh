#!/bin/bash
#$ -cwd

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi

echo "============== 05-2 bamtools filter ================"
echo " "

echo "Filtering for proper paired .... starting at:"
date


${bamtools} filter \
   -isMapped true \
   -isPaired true \
   -isProperPair true \
   -in  $PWDS/${subjectID}.fxmt.bam \
   -out $PWDS/${subjectID}.fxmt.flt.bam

${bamtools} stats \
   -insert \
   -in $PWDS/${subjectID}.fxmt.flt.bam \
   > $PWDS/${subjectID}.fxmt.flt.stats



${samtools} index $PWDS/${subjectID}.fxmt.flt.bam


echo "Filtering output: ${subjectID}.filtered.ontarget.bam .... done at:"
date




