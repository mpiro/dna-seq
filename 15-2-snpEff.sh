#!/bin/bash

run_script="/md2/01-script/illuminizer_v8_bsi"
GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi

# . ${run_script}/15-2-snpEff.sh $working_dir_path  $subjectID
# . ${run_script}/15-2-snpEff.sh  /md1/BSI-SZ/analysis/SZ1_analysis   C05EJACXX_1_ACAGTGs_1

subjectDir=$1
subjectID=$2

PWDS="$subjectDir/$2/analysis"
vargtkdir="$PWDS/variants-gatk"

java -Xmx${heap}m  \
  -jar   ${snpEff}/snpEff.jar  \
   eff   \
   -v -i vcf \
   -o vcf   \
   -s $vargtkdir/${subjectID}.snps.PASS.snpEff.summary.html  \
   -c    ${snpEff}/snpEff.config \
   GRCh37.65   \
   $vargtkdir/${subjectID}.snps.PASS.vcf   
 >  $vargtkdir/${subjectID}.snps.PASS.snpEff.annotate.vcf
 
java -Xmx${heap}m  \
  -jar   ${snpEff}/snpEff.jar  \
   eff   \
   -v -i vcf \
   -o vcf   \
   -s $vargtkdir/${subjectID}.indels.PASS.snpEff.summary.html  \
   -c    ${snpEff}/snpEff.config \
   GRCh37.65   \
   $vargtkdir/${subjectID}.indels.PASS.vcf
 >  $vargtkdir/${subjectID}.indels.PASS.snpEff.annotate.vcf






