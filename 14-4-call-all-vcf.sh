#!/bin/sh
#$ -S /bin/sh
#$ -cwd
#$ -l h_vmem=5G,time=8::

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi

echo "============== 14-4 GATK call all vcf  ================"
echo " "

rm -f $subjectDir/*.idx

java -Xmx${heap}m  \
    -jar $gatk \
    -R $REF \
    -T UnifiedGenotyper \
    --dbsnp $DBSNP \
    -I $PWDS/${subjectID}.realigned.recal.bam \
    --out $PWDS/${subjectID}.snps.raw.all.vcf \
    -stand_call_conf 30.0 \
    -stand_emit_conf 10.0 \
    -l INFO \
    -A DepthOfCoverage \
    -A HaplotypeScore \
    -A InbreedingCoeff \
    -glm SNP  \
    -nt 1  \
    -L $ExonFile \
   -U ALLOW_UNINDEXED_BAM \
   --output_mode EMIT_ALL_CONFIDENT_SITES

 

java -Xmx${heap}m  \
  -jar $gatk \
  -l INFO -T VariantFiltration \
  -R $REF \
  -o $PWDS/${subjectID}.snps.all.vcf \
  --variant $PWDS/${subjectID}.snps.raw.all.vcf \
  --mask $PWDS/${subjectID}.indels.vcf \
  --maskName InDel \
  --clusterSize 3 \
  --clusterWindowSize 10 \
  --filterExpression "QD < 2.0" \
  --filterName "QDFilter" \
  --filterExpression "MQ < 40.0" \
  --filterName "MQFilter" \
  --filterExpression "FS > 60.0" \
  --filterName "FSFilter" \
  --filterExpression "HaplotypeScore > 13.0" \
  --filterName "HaplotypeScoreFilter" \
  --filterExpression "MQRankSum < -12.5" \
  --filterName "MQRankSumFilter" \
  --filterExpression "ReadPosRankSum < -8.0" \
  --filterName "ReadPosRankSumFilter" \
  --filterExpression "QUAL < 30.0 || DP < 6 || DP > 5000 || HRun > 5" \
  --filterName "StandardFilters" \
  --filterExpression "MQ0 >= 4 && ((MQ0 / (1.0 * DP)) > 0.1)" \
  --filterName "HARD_TO_VALIDATE"

mline=`grep -n "#CHROM" $PWDS/${subjectID}.snps.vcf | cut -d':' -f 1`
head -n $mline $PWDS/${subjectID}.snps.vcf > $PWDS/head.vcf
cat $PWDS/${subjectID}.snps.vcf | grep PASS | cat $PWDS/head.vcf - > $PWDS/${subjectID}.snps.PASS.vcf

rm -f $PWDS/head.vcf
rm -f $PWDS/${subjectID}.snps.vcf
rm -f $PWDS/${subjectID}.snps.raw.all.vcf
rm -f $subjectDir/*.idx

