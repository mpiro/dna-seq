#!/bin/sh
#$ -S /bin/sh
#$ -cwd
#$ -l h_vmem=5G,time=8::

# Recalibrate base quality score

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi

echo "============== 12-2 GATK recalibrate  ================"
echo " "

date
mkdir -p ${tmp_folder}_recal 

echo -e " Walking through the BAM file and rewrite the quality scores "

java -Xmx${heap}m -Djava.io.tmpdir\=${tmp_folder}_recal \
 -jar $gatk \
 -l INFO \
 -R $REF \
 -I $PWDS/${subjectID}.realigned.srt.bam \
 -T TableRecalibration \
 --default_platform Illumina \
  --default_read_group MP1  \
 --out $PWDS/${subjectID}.realigned.recal.bam \
 -recalFile $PWDS/${subjectID}.flt.recal_v1.csv  \
    -L $ExonFile


$samtools index $PWDS/${subjectID}.realigned.recal.bam

echo -e "Recaliration done at :"
date


