#!/bin/sh
#$ -S /bin/sh
#$ -cwd
#$ -l h_vmem=5G,time=8::

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi

echo "============== 12-3 GATK recalc analyzeCovar_v2  ================"
echo " "


echo -e "Determine (or 'count') the covariates affecting base quality scores \
 in the initial BAM file, emitting a CSV file when done.  \n"

mkdir -p ${tmp_folder}_covar

 java -Xmx${heap}m -Djava.io.tmpdir\=${tmp_folder}_covar \
  -jar $gatk \
  -R $REF \
  -I $PWDS/${subjectID}.realigned.recal.bam \
  -knownSites ${DBSNP} \
  -T CountCovariates \
  -nt $CPUs \
  -cov ReadGroupCovariate \
  -cov QualityScoreCovariate \
  -cov CycleCovariate \
  -cov DinucCovariate \
  -recalFile $PWDS/${subjectID}.flt.recal_v2.csv  \
    -L $ExonFile


 mkdir -p  $PWDS/analyzeCovar_v2
 
 java -Xmx${heap}m  \
  -jar ${gatk_analyzecovar} \
  -recalFile $PWDS/${subjectID}.flt.recal_v2.csv  \
  -outputDir $PWDS/analyzeCovar_v2  \
  -ignoreQ 3






