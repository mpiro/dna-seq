#!/bin/bash
#$ -cwd

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi

echo "============== 04-samtools merging ================"
echo " "

echo "Merging started at:"
date

countlanes=`ls -l ${PWDS}/${subjectID}.*.aligned.srt.bam | wc -l`

mkdir ${PWDS}/alignments
mv ${PWDS}/${subjectID}.* ${PWDS}/alignments/
mv ${PWDS}/fastq* ${PWDS}/alignments/

if [[ $countlanes > 1 ]] ;  then echo "merging $countlanes bams"; else echo "moving bam"; fi

if [[ $countlanes > 1 ]] 

then
	# merge bams
	${samtools} merge \
           ${PWDS}/${subjectID}.bam \
           ${PWDS}/alignments/${subjectID}.*.aligned.srt.bam
else
	mv ${PWDS}/alignments/${subjectID}.1.aligned.srt.bam \
           ${PWDS}/${subjectID}.bam	

fi



echo "... to ${subjectID}.bam .... done! at:"
date



 
