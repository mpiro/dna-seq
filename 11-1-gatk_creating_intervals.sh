#!/bin/sh
#$ -S /bin/sh
#$ -cwd
#$ -l h_vmem=5G,time=8::

GLOBAL="${run_script}/global_config.sh"

if [[ -e $GLOBAL ]]
then
        . $GLOBAL
else
echo "Global config file not found. Exiting."
        exit 1
fi

echo "============== 11-1 GATK Creating intervals  ================"
echo " "

echo "11-1 Creating intervals .... determine suspicious intervals which are likely in need of realignment completed! ... starting at:"
date


#  -U ALLOW_UNINDEXED_BAM \


# First, we determine (small) suspicious intervals which are likely in need of realignment.

mkdir -p ${tmp_folder}_realign
# rm -f ${DBSNP_idx}

java -Xmx${heap}m -Djava.io.tmpdir\=${tmp_folder}_realign \
  -jar $gatk \
  -T RealignerTargetCreator \
  -I $PWDS/${subjectID}.mq.fxmt.bam \
  -R $REF \
  -known $DBSNP  \
  -nt $CPUs \
  -o $PWDS/${subjectID}.forRealign.intervals   \
    -L $ExonFile

#   --default_platform Illumina   \
#   --default_read_group MP1  \

#  -D $DBSNP

# rm -f ${DBSNP_idx}
rm -rf ${tmp_folder}_realign
 
echo "Creating intervals ... done at:"
date


